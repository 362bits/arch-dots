syntax on
set number
set cursorline
set nowrap
set showmatch
set nobackup
set hlsearch
set smartcase
set ignorecase
set showcmd
set history=500
set wildmenu
set wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.jpeg,*.dat,*.pdf

set autoindent
set tabstop=4
set shiftwidth=4
set smartindent
set smarttab
set softtabstop=4
set expandtab
set mouse=a

let g:airline_powerline_fonts = 1
set ruler
"set rtp+=/usr/share/powerline/bindings/vim
call plug#begin()
    Plug 'preservim/nerdtree'
    Plug 'dense-analysis/ale'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'vim-airline/vim-airline'
    Plug 'morhetz/gruvbox'
    Plug 'ryanoasis/vim-devicons'
    Plug 'SirVer/ultisnips'
    Plug 'honza/vim-snippets'
    Plug 'mhinz/vim-startify'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'dracula/vim'
    Plug 'joshdick/onedark.vim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'Shatur/neovim-cmake'
    Plug 'mfussenegger/nvim-dap'
    Plug 'ycm-core/YouCompleteMe'
call plug#end()

set ttyfast
let g:ale_completion_enabled = 1
let g:onedark_color_overrides = {
\ "background": {"gui": "#191919", "cterm": "235", "cterm16": "0" },
\ "purple": { "gui": "#C678DF", "cterm": "170", "cterm16": "5" }
\}
let g:onedark_terminal_italics = 1
"colorscheme gruvbox
colorscheme onedark

let mapleader='\\'
noremap <space> :
noremap o o<esc>i
noremap O O<esc>
noremap Y y$
noremap <f5> :w <CR>:!clear <CR>:!python % <CR>
noremap <s-j> ddp
noremap <s-k> ddkkp
noremap <s-tab> :NERDTreeToggle<cr>
noremap <c-t> :vnew
set fillchars+=vert:\▏
let g:ale_c_build_dir = "./build"                                                
let g:ale_c_parse_makefile = 1   
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
"if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
"endif
let g:ale_cpp_cc_options = '-std=c++20 -Wall'
